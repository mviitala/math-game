import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  
  constructor(public alertController: AlertController){

  }

  number1 : number;
  number2 : number;
  answer : number;
  
  ngOnInit(): void {
    
    this.randomizeNumbers();
  }

  async calculate(){

    const result = this.number1 + this.number2;

    if (this.answer == result){
      
      this.showMessage("Correct Answer!");
    }
    else{
      this.showMessage("Incorrect Answer!");
    }
    this.randomizeNumbers();    
  }

  private async showMessage(m : string){

    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: m,
      buttons: ['OK']
    });

    await alert.present();
  }

  private randomizeNumbers() : void{

    this.number1 = Math.floor(Math.random() * 10) + 1;
    this.number2 = Math.floor(Math.random() * 10) + 1;
    this.answer = 0;
  }

}
